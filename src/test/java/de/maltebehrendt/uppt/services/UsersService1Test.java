package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.junit.AbstractTest;
import de.maltebehrendt.uppt.util.EncryptionUtils;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.invoke.MethodHandles;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class UsersService1Test extends AbstractTest {
    // TODO: test what happens if user removed in meantime and session start/end
    // TODO: test invalid requests, security violations, ...

    @BeforeClass
    public static void setup(TestContext context) {
        setupTestVertxAndDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }

    @AfterClass
    public static void teardown(TestContext context) {
        //tearDownTestVertxAndCleanupDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }

    @Test
    public void testProcessMissingUserRegistrationWithoutRolesMinimum(TestContext context) {
        Async testResult = context.async(1);

        String emailAddress = getRandomMailAddress();

        JsonObject authMethodConfig = new JsonObject()
                .put("type", "email")
                .put("emailAddress", emailAddress)
                .put("hashFactory", "PBKDF2WithHmacSHA512")
                .put("hashSalt", "testSalt")
                .put("hashIterations", 10)
                .put("hashLength", 256);

        String passwordHash = EncryptionUtils.hashForKey("someTestPW", authMethodConfig.getString("hashFactory"),authMethodConfig.getString("hashSalt"), authMethodConfig.getInteger("hashIterations"), authMethodConfig.getInteger("hashLength"));
        authMethodConfig.put("passwordHash", passwordHash);

        send("users", "1", "missingUserRegistration", new JsonObject()
                        .put("authMethodConfig", authMethodConfig)
                , reply -> {
                    context.assertTrue(reply.succeeded());
                    context.assertEquals(200, reply.statusCode());

                    JsonObject result = reply.getBodyAsJsonObject();
                    context.assertNotNull(result.getString("userID"));
                    context.assertEquals("active", result.getString("status"));
                    context.assertEquals(0, result.getJsonArray("userRoles").size());
                    //context.assertTrue(result.getJsonArray("userRoles").contains("authenticated"));

                    testResult.countDown();
                });

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessMissingUserRegistrationWithRolesMinimum(TestContext context) {
        Async testResult = context.async(1);

        String emailAddress = getRandomMailAddress();

        JsonObject authMethodConfig = new JsonObject()
                .put("type", "email")
                .put("emailAddress", emailAddress)
                .put("hashFactory", "PBKDF2WithHmacSHA512")
                .put("hashSalt", "testSalt")
                .put("hashIterations", 10)
                .put("hashLength", 256);

        String passwordHash = EncryptionUtils.hashForKey("someTestPW", authMethodConfig.getString("hashFactory"),authMethodConfig.getString("hashSalt"), authMethodConfig.getInteger("hashIterations"), authMethodConfig.getInteger("hashLength"));
        authMethodConfig.put("passwordHash", passwordHash);

        send("users", "1", "missingUserRegistration", new JsonObject()
                        .put("authMethodConfig", authMethodConfig)
                        .put("userRoles", new JsonArray().add("testUser").add("tester"))
                , reply -> {
                    context.assertTrue(reply.succeeded());
                    context.assertEquals(200, reply.statusCode());

                    JsonObject result = reply.getBodyAsJsonObject();
                    context.assertNotNull(result.getString("userID"));
                    context.assertEquals("active", result.getString("status"));
                    context.assertEquals(2, result.getJsonArray("userRoles").size());
                    context.assertTrue(result.getJsonArray("userRoles").contains("testUser"));
                    context.assertTrue(result.getJsonArray("userRoles").contains("tester"));

                    testResult.countDown();
                });

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessMissingAuthMethodConfigForEmail(TestContext context) {
        Async testResult = context.async(1);

        Future<JsonObject> userRegistrationFuture = Future.future();
        userRegistrationFuture.setHandler(registrationResult -> {
            context.assertFalse(registrationResult.failed());

            JsonObject user = registrationResult.result();

            send("users", "1", "missingAuthMethodConfig", new JsonObject()
                            .put("authMethodIdentifier", new JsonObject()
                                .put("type", "email")
                                .put("emailAddress", user.getString("emailAddress")))
                    , reply -> {
                        context.assertTrue(reply.succeeded());
                        context.assertEquals(200, reply.statusCode());

                        JsonObject result = reply.getBodyAsJsonObject();
                        context.assertTrue(result.containsKey("authMethodConfig"));

                        JsonObject authMethodConfig = result.getJsonObject("authMethodConfig");
                        context.assertFalse(authMethodConfig.isEmpty());
                        context.assertNotNull(authMethodConfig.getString("hashFactory"));
                        context.assertNotNull(authMethodConfig.getString("hashSalt"));
                        context.assertNotNull(authMethodConfig.getInteger("hashIterations"));
                        context.assertNotNull(authMethodConfig.getInteger("hashLength"));
                        context.assertEquals("unverified", authMethodConfig.getString("status"));

                        testResult.countDown();
                    });

        });
        getNewUser(null, userRegistrationFuture);

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessMissingAuthMethodConfigForUnknownEmail(TestContext context) {
        Async testResult = context.async(1);

        Future<JsonObject> userRegistrationFuture = Future.future();
        userRegistrationFuture.setHandler(registrationResult -> {
            context.assertFalse(registrationResult.failed());

            JsonObject user = registrationResult.result();

            send("users", "1", "missingAuthMethodConfig", new JsonObject()
                            .put("authMethodIdentifier", new JsonObject()
                                    .put("type", "email")
                                    .put("emailAddress", user.getString("emailAddress")+1))
                    , reply -> {
                        context.assertTrue(reply.succeeded());
                        context.assertEquals(404, reply.statusCode());

                        testResult.countDown();
                    });

        });
        getNewUser(null, userRegistrationFuture);

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessMissingSessionRegistrationAnonymousMinimum(TestContext context) {
        Async testResult = context.async(1);

        JsonObject sessionTokenHashConfig = new JsonObject()
                .put("hashFactory", "PBKDF2WithHmacSHA512")
                .put("hashSalt", "testSalt")
                .put("hashIterations", 10)
                .put("hashLength", 256);

        String token = java.util.Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
        // skipping calculating a hash here

        send("users", "1", "missingSessionRegistration", new JsonObject()
                        .put("sessionTokenHash", token)
                        .put("sessionTokenHashConfig", sessionTokenHashConfig)
                , reply -> {
                    context.assertTrue(reply.succeeded());
                    context.assertEquals(200, reply.statusCode());

                    testResult.countDown();
                });

        testResult.awaitSuccess();
    }

    //@Test
    public void testProcessMissingSessionAuthenticationWrongPW(TestContext context) {
        Async testResult = context.async(2);

        JsonObject hashConfig = new JsonObject()
                .put("hashFactory", "PBKDF2WithHmacSHA512")
                .put("hashSalt", "testSalt")
                .put("hashIterations", 10)
                .put("hashLength", 256);

        String token = java.util.Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
        // skipping calculating a hash here

        send("users", "1", "missingSessionRegistration", new JsonObject()
                        .put("sessionTokenHash", token)
                        .put("sessionTokenHashConfig", hashConfig)
                , sessionReply -> {
                    context.assertTrue(sessionReply.succeeded());
                    context.assertEquals(200, sessionReply.statusCode());
                    testResult.countDown();

                    Future<JsonObject> userRegistrationFuture = Future.future();
                    userRegistrationFuture.setHandler(registrationResult -> {
                        context.assertTrue(registrationResult.succeeded());
                        JsonObject user = registrationResult.result();

                        send("users", "1", "missingSessionAuthentication", new JsonObject()
                                        .put("sessionTokenHash", token)
                                        .put("authMethodConfig", hashConfig
                                                .put("type", "email")
                                                .put("emailAddress", user.getString("emailAddress"))
                                                .put("passwordHash", user.getString("passwordHash")+2))
                                , authReply -> {
                                    context.assertTrue(authReply.succeeded());
                                    context.assertEquals(401, authReply.statusCode());
                                    testResult.countDown();
                                });
                    });
                    getNewUser(null, userRegistrationFuture);
                });

        testResult.awaitSuccess();
    }

    //@Test
    public void testProcessMissingSessionAuthenticationWrongMail(TestContext context) {
        Async testResult = context.async(2);

        JsonObject hashConfig = new JsonObject()
                .put("hashFactory", "PBKDF2WithHmacSHA512")
                .put("hashSalt", "testSalt")
                .put("hashIterations", 10)
                .put("hashLength", 256);

        String token = java.util.Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
        // skipping calculating a hash here

        send("users", "1", "missingSessionRegistration", new JsonObject()
                        .put("sessionTokenHash", token)
                        .put("sessionTokenHashConfig", hashConfig)
                , sessionReply -> {
                    context.assertTrue(sessionReply.succeeded());
                    context.assertEquals(200, sessionReply.statusCode());
                    testResult.countDown();

                    Future<JsonObject> userRegistrationFuture = Future.future();
                    userRegistrationFuture.setHandler(registrationResult -> {
                        context.assertTrue(registrationResult.succeeded());
                        JsonObject user = registrationResult.result();

                        send("users", "1", "missingSessionAuthentication", new JsonObject()
                                        .put("sessionTokenHash", token)
                                        .put("authMethodConfig", hashConfig
                                                .put("type", "email")
                                                .put("emailAddress", getRandomMailAddress())
                                                .put("passwordHash", user.getString("passwordHash")))
                                , authReply -> {
                                    context.assertTrue(authReply.succeeded());
                                    context.assertEquals(401, authReply.statusCode());
                                    testResult.countDown();
                                });
                    });
                    getNewUser(null, userRegistrationFuture);
                });

        testResult.awaitSuccess();
    }

    //@Test
    public void testProcessMissingSessionAuthenticationNoUserRoles(TestContext context) {
        Async testResult = context.async(2);

        JsonObject hashConfig = new JsonObject()
                .put("hashFactory", "PBKDF2WithHmacSHA512")
                .put("hashSalt", "testSalt")
                .put("hashIterations", 10)
                .put("hashLength", 256);

        String token = java.util.Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
        // skipping calculating a hash here

        send("users", "1", "missingSessionRegistration", new JsonObject()
                        .put("sessionTokenHash", token)
                        .put("sessionTokenHashConfig", hashConfig)
                , sessionReply -> {
                    context.assertTrue(sessionReply.succeeded());
                    context.assertEquals(200, sessionReply.statusCode());
                    testResult.countDown();

                    Future<JsonObject> userRegistrationFuture = Future.future();
                    userRegistrationFuture.setHandler(registrationResult -> {
                        context.assertTrue(registrationResult.succeeded());
                        JsonObject user = registrationResult.result();

                        send("users", "1", "missingSessionAuthentication", new JsonObject()
                                        .put("sessionTokenHash", token)
                                        .put("authMethodConfig", hashConfig
                                                .put("type", "email")
                                                .put("emailAddress", user.getString("emailAddress"))
                                                .put("passwordHash", user.getString("passwordHash")))
                                , authReply -> {
                            context.assertTrue(authReply.succeeded());
                            context.assertEquals(200, authReply.statusCode());

                            JsonObject result = authReply.getBodyAsJsonObject();
                            context.assertNotNull(result.getString("userID"));
                            context.assertNotNull(result.getJsonArray("userRoles"));
                            context.assertEquals(0, result.getJsonArray("userRoles").size());
                            testResult.countDown();
                        });
                    });
                    getNewUser(null, userRegistrationFuture);
                });

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessMissingSessionAuthenticationWithUserRoles(TestContext context) {
        Async testResult = context.async(2);

        JsonObject hashConfig = new JsonObject()
                .put("hashFactory", "PBKDF2WithHmacSHA512")
                .put("hashSalt", "testSalt")
                .put("hashIterations", 10)
                .put("hashLength", 256);

        String token = java.util.Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
        // skipping calculating a hash here

        send("users", "1", "missingSessionRegistration", new JsonObject()
                        .put("sessionTokenHash", token)
                        .put("sessionTokenHashConfig", hashConfig)
                , sessionReply -> {
                    context.assertTrue(sessionReply.succeeded());
                    context.assertEquals(200, sessionReply.statusCode());
                    testResult.countDown();

                    Future<JsonObject> userRegistrationFuture = Future.future();
                    userRegistrationFuture.setHandler(registrationResult -> {
                        context.assertTrue(registrationResult.succeeded());
                        JsonObject user = registrationResult.result();

                        System.err.println("should have roles");
                        System.err.println(user.encodePrettily());

                        send("users", "1", "missingSessionAuthentication", new JsonObject()
                                        .put("sessionTokenHash", token)
                                        .put("authMethodConfig", hashConfig
                                                .put("type", "email")
                                                .put("emailAddress", user.getString("emailAddress"))
                                                .put("passwordHash", user.getString("passwordHash")))
                                , authReply -> {
                                    context.assertTrue(authReply.succeeded());
                                    context.assertEquals(200, authReply.statusCode());

                                    JsonObject result = authReply.getBodyAsJsonObject();
                                    context.assertNotNull(result.getString("userID"));
                                    context.assertNotNull(result.getJsonArray("userRoles"));
                                    context.assertEquals(0, result.getJsonArray("userRoles").size());
                                    testResult.countDown();
                                });
                    });
                    getNewUser(new JsonArray().add("tester").add("testAdmin"), userRegistrationFuture);
                });

        testResult.awaitSuccess();
    }



    private void getNewUser(JsonArray userRoles, Future<JsonObject> future) {
        String emailAddress = getRandomMailAddress();

        JsonObject authMethodConfig = new JsonObject()
                .put("type", "email")
                .put("emailAddress", emailAddress)
                .put("hashFactory", "PBKDF2WithHmacSHA512")
                .put("hashSalt", "testSalt")
                .put("hashIterations", 10)
                .put("hashLength", 256);

        String passwordHash = EncryptionUtils.hashForKey("someTestPW", authMethodConfig.getString("hashFactory"),authMethodConfig.getString("hashSalt"), authMethodConfig.getInteger("hashIterations"), authMethodConfig.getInteger("hashLength"));
        authMethodConfig.put("passwordHash", passwordHash);

        if(userRoles == null) {
            userRoles = new JsonArray();
        }

        send("users", "1", "missingUserRegistration", new JsonObject()
                        .put("authMethodConfig", authMethodConfig)
                        .put("userRoles", userRoles)
                , reply -> {
                    if(reply.failed()) {
                        future.fail(reply.cause());
                        return;
                    }
                    if(reply.statusCode() != 200) {
                        future.fail(reply.getMessage());
                    }

                    JsonObject result = reply.getBodyAsJsonObject();
                    result.put("emailAddress", emailAddress);
                    result.put("passwordHash", passwordHash);

                    future.complete(result);
                });
    }

    private String getRandomMailAddress() {
        return UUID.randomUUID().toString() + "@test.de";
    }


}
