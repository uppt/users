package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.annotation.Payload;
import de.maltebehrendt.uppt.annotation.Processor;
import de.maltebehrendt.uppt.enums.DataType;
import de.maltebehrendt.uppt.events.Message;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import javax.crypto.SecretKey;
import javax.crypto.spec.PBEKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

public class UsersService1 extends AbstractService {

    private static String aql_registerUserByEmail = "LET user = (INSERT @userDocument IN users RETURN {key: NEW._key, id: NEW._id, userID: NEW.userID}) LET notification = (INSERT @notificationDocument IN notificationSettings RETURN {id: NEW._id}) LET profile = (INSERT @profileDocument IN profiles RETURN {id: NEW._id}) LET auth = (INSERT @authenticationDocument IN authenticationMethods RETURN {id: NEW._id}) LET authEdge = (INSERT {_from: FIRST(user).id, _to: FIRST(auth).id, type: @authType} INTO has_authentication_method RETURN {id: NEW._id}) LET profileEdge = (INSERT {_from: FIRST(user).id, _to: FIRST(profile).id, type: @origin} INTO has_profile RETURN {id: NEW._id}) LET notificationEdge = (INSERT {_from: FIRST(user).id, _to: FIRST(notification).id, type: @notificationType} INTO has_notification_settings RETURN {id: NEW._id}) RETURN {user: FIRST(user), notification: FIRST(notification), profile: FIRST(profile), auth: FIRST(auth), authEdge: FIRST(authEdge), profileEdge: FIRST(profileEdge), notificationEdge: FIRST(notificationEdge)}";
    private static String aql_registerUserByEmailWithRolesOTF = "LET user = (INSERT @userDocument IN users RETURN {key: NEW._key, id: NEW._id, userID: NEW.userID}) LET notification = (INSERT @notificationDocument IN notificationSettings RETURN {id: NEW._id}) LET profile = (INSERT @profileDocument IN profiles RETURN {id: NEW._id}) LET auth = (INSERT @authenticationDocument IN authenticationMethods RETURN {id: NEW._id}) LET roles = (FOR role IN @roleNames UPSERT {name: role} INSERT {name: role, lastChange: DATE_NOW()} UPDATE {lastChange: DATE_NOW()} IN roles RETURN {name: NEW.name, id: NEW._id}) LET authEdge = (INSERT {_from: FIRST(user).id, _to: FIRST(auth).id, type: @authType} INTO has_authentication_method RETURN {id: NEW._id}) LET profileEdge = (INSERT {_from: FIRST(user).id, _to: FIRST(profile).id, type: @origin} INTO has_profile RETURN {id: NEW._id}) LET notificationEdge = (INSERT {_from: FIRST(user).id, _to: FIRST(notification).id, type: @notificationType} INTO has_notification_settings RETURN {id: NEW._id}) LET roleEdges = (FOR role IN roles INSERT {_from: FIRST(user).id, _to: role.id, created: DATE_NOW(), origin: @origin} INTO has_role RETURN {id: NEW._id}) RETURN {user: FIRST(user), notification: FIRST(notification), profile: FIRST(profile), auth: FIRST(auth), roles: roles, authEdge: FIRST(authEdge), profileEdge: FIRST(profileEdge), notificationEdge: FIRST(notificationEdge), roleEdges: roleEdges}";
    private static String aql_checkIfEmailAddressIsUsed = "LET byNotifications = (FOR n IN notificationSettings FILTER n.emailAddress == @emailAddress RETURN {id: n._id}) LET byEmailAuths = (FOR a IN authenticationMethods FILTER a.emailAddress == @emailAddress RETURN {id: a._id}) RETURN {byNotifications: byNotifications, byEmailAuths: byEmailAuths}";
    private static String aql_getAuthMethodConfigByEmail ="FOR a IN authenticationMethods FILTER a.emailAddress == @emailAddress LIMIT 1 RETURN {hashFactory: a.hashFactory, hashSalt: a.hashSalt, hashIterations: a.hashIterations, hashLength: a.hashLength, status: a.status}";

    // TODO: extend to traverse roles
    private static String aql_getUserOfSessionByTokenHash = "FOR user IN INBOUND @tokenHashAsID has_session LIMIT 1 RETURN {id: user._id, key: user._key}";
    private static String aql_registerAnonymousSession = "LET session = (INSERT @sessionDocument INTO sessions RETURN {key: NEW._key, id: NEW._id})  LET sessionEdge = (INSERT {_from: @userDocumentID, _to: FIRST(session).id} INTO has_session RETURN {key: NEW._key})  RETURN {sessionKey: FIRST(session).key, edgeKey: first(sessionEdge).key}";
    private static String aql_authenticateSession = "LET auth = (FOR a IN authenticationMethods FILTER a.emailAddress == @emailAddress AND a.passwordHash == @passwordHash LIMIT 1 RETURN {id: a._id}) LET userDetails = (FOR r,e,p IN 1..2 INBOUND FIRST(auth).id has_authentication_method, OUTBOUND has_role RETURN {name: r.name, userID: p.vertices[1].userID}) RETURN {authID: FIRST(auth).id, userDetails: userDetails}";
    private Pattern emailPattern = null;

    @Processor(
            domain = "users",
            version = "1",
            type = "missingSessionAuthentication",
            description = "Either registers a new, anonymous session or - if a session token hash is provided - performs a session handover",
            requires = {
                    @Payload(key = "sessionTokenHash", type = DataType.STRING, description = "Either hash of an existing session token or the session token hash to be set"),
                    @Payload(key = "authMethodConfig", type = DataType.JSONObject, description = "Must contain string 'type' denoting authentication method (currently only 'email' is supported and all required parameters, e.g. password hash, email address, ...")
            },
            provides = {
                    @Payload(key = "userID", type = DataType.STRING, description = "ID of the user that started the session"),
                    @Payload(key = "userRoles", type = DataType.STRING, description = "Roles of the user that started the session")
            }
    )
    public void processMissingSessionAuthentication(Message message) {
        JsonObject body = message.getBodyAsJsonObject();

        String sessionTokenHash = body.getString("sessionTokenHash");
        JsonObject authenticationMethod = body.getJsonObject("authMethodConfig");
        if(sessionTokenHash == null || sessionTokenHash.isEmpty()) {
            message.fail(400, "Must provide non-empty sessionTokenHash");
            return;
        }
        if(authenticationMethod == null || !authenticationMethod.containsKey("type")) {
            message.fail(400, "Must provide authMethodConfig");
            return;
        }

        String authenticationType = authenticationMethod.getString("type");
        if("email".equals(authenticationType)) {
            String emailAddress = authenticationMethod.getString("emailAddress");
            String passwordHash = authenticationMethod.getString("passwordHash");

            if (emailAddress == null) {
                message.fail(400, "Must provide 'emailAddress' for identifying the settings for the user");
                return;
            }
            if(passwordHash == null || passwordHash.isEmpty()) {
                message.fail(400, "authenticationMethod must contain entry 'passwordHash' with the user's password hash");
                return;
            }

            Future<JsonArray> authFuture = Future.future();
            authFuture.setHandler(authResult -> {
                if(authResult.failed()) {
                    logger.error(message.correlationID(), emailAddress, 500, "Failed to check user authentication", authResult.cause());
                    message.fail("Failed to check authentication for an unknown reason");
                    return;
                }

                JsonObject queryResult = authResult.result().getJsonObject(0);
                if(queryResult == null || queryResult.isEmpty() || !queryResult.containsKey("authID") || queryResult.getString("authID").isEmpty()) {
                    logger.security(message.correlationID(), emailAddress, 500, "Failed authentication attempt");
                    message.fail(401, "Email address not registered or password incorrect");
                    return;
                }

                System.err.println(authResult.result().encodePrettily());

                JsonArray userDetails = queryResult.getJsonArray("userDetails");
                String userID = userDetails.getJsonObject(0).getString("userID");

                message.reply(new JsonObject().put("userID", userID).put("userRoles", new JsonArray()));
            });
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("emailAddress", emailAddress);
            paramMap.put("passwordHash", passwordHash);
            database.query(aql_authenticateSession, paramMap, authFuture);

        }
        else {
            logger.warn(message.correlationID(), message.origin(), 501, "Unknown authentication type : " + authenticationType);
            message.fail(501, "AuthType is not known/supported.");
            return;
        }
    }


    @Processor(
            domain = "users",
            version = "1",
            type = "missingSessionRegistration",
            description = "Either registers a new, anonymous session or - if a session token hash is provided - performs a session handover",
            requires = {
                    @Payload(key = "sessionTokenHash", type = DataType.STRING, description = "Either hash of an existing session token or the session token hash to be set"),
                    @Payload(key = "sessionTokenHashConfig", type = DataType.JSONObject, description = "Hashing configuration, i.e. hashFactory, hashSalt, hashIterations, hashLength")
            },
            optional = {
                    @Payload(key = "sessionContext", type = DataType.JSONObject, description = "Details on the session context, e.g. router, device infos, ..."),
                    @Payload(key = "sessionConfig", type = DataType.JSONObject, description = "Configuration of the session, includes in particular the router's address and set traits")
            },
            provides = {
                    @Payload(key = "userID", type = DataType.STRING, description = "ID of the user that started the session"),
                    @Payload(key = "userRoles", type = DataType.STRING, description = "Roles of the user that started the session"),
                    @Payload(key = "sessionTokenHash", type = DataType.STRING, description = "Hash of the session token (changed if a session handover was performed)")
            }
    )
    public void processMissingSessionRegistration(Message message) {
        JsonObject body = message.getBodyAsJsonObject();

        String sessionTokenHash = body.getString("sessionTokenHash");
        JsonObject sessionTokenHashConfig = body.getJsonObject("sessionTokenHashConfig");
        if(sessionTokenHashConfig == null || sessionTokenHashConfig.isEmpty()) {
            message.fail(400, "Must provide non-empty sessionTokenHashConfig");
            return;
        }
        if(sessionTokenHash == null || sessionTokenHash.isEmpty()) {
            message.fail(400, "Must provide non-empty sessionTokenHash");
            return;
        }

        String hashFactory = sessionTokenHashConfig.getString("hashFactory");
        String hashSalt = sessionTokenHashConfig.getString("hashSalt");
        Integer hashIterations = sessionTokenHashConfig.getInteger("hashIterations");
        Integer hashLength = sessionTokenHashConfig.getInteger("hashLength");
        if(hashFactory == null || hashFactory.isEmpty()) {
            message.fail(400, "Must provide non-empty sessionTokenHashConfig.hashFactory");
            return;
        }
        if(hashSalt == null || hashSalt.isEmpty()) {
            message.fail(400, "Must provide non-empty sessionTokenHashConfig.hashSalt");
            return;
        }
        if(hashIterations == null || hashIterations < config().getJsonObject("hashPolicy").getInteger("minimumIterations")) {
            message.fail(400, "sessionTokenHashConfig.hashIterations is smaller than required minimum of " + config().getJsonObject("hashPolicy").getInteger("minimumIterations"));
            return;
        }
        if(hashLength == null || hashLength < config().getJsonObject("hashPolicy").getInteger("minimumLength")) {
            message.fail(400, "sessionTokenHashConfig.hashLength is smaller than required minimum of " + config().getJsonObject("hashPolicy").getInteger("minimumLength"));
            return;
        }

        JsonObject sessionContext = body.containsKey("sessionContext") ? body.getJsonObject("sessionContext") : new JsonObject();
        JsonObject sessionConfig =  body.containsKey("sessionContext") ? body.getJsonObject("sessionConfig") : new JsonObject();

        // ensuring the token is a valid key for ArangoDB...
        String sessionTokenHashAsKey = sessionTokenHash.replace("/", "_");
        String sessionTokenHashAsID = "sessions/" + sessionTokenHashAsKey;

        Future<JsonArray> checkTokenFuture = Future.future();
        checkTokenFuture.setHandler(checkTokenResult -> {
            if(checkTokenResult.failed()) {
                logger.error(message.correlationID(), message.origin(), 500, "Failed to check session token", checkTokenResult.cause());
                message.fail("Failed to check token for an unknown reason");
                return;
            }

            JsonObject result = checkTokenResult.result().isEmpty()? null : checkTokenResult.result().getJsonObject(0);

            if(result == null) {
                // register anonymous session
                JsonObject sessionDocument = new JsonObject()
                        .put("_key", sessionTokenHashAsKey)
                        .put("startTimestamp", System.currentTimeMillis())
                        .put("interruptedTimestamp", 0L)
                        .put("tokenHash", sessionTokenHash)
                        .put("tokenHashConfig", sessionTokenHashConfig)
                        .put("context", sessionContext)
                        .put("config", sessionConfig);

                Future<JsonArray> sessionFuture = Future.future();
                sessionFuture.setHandler(sessionResult -> {
                    if(sessionResult.failed()) {
                        String errorMessage = sessionResult.cause().getMessage();
                        if(errorMessage != null && errorMessage.contains("409") && errorMessage.contains("1210")) {
                            message.fail(409, "Session was closed and is not valid anymore");
                        }
                        else {
                            message.fail("Failed to create anonymous session for an unknown reason");
                        }
                        logger.error(message.correlationID(), message.origin(), 500, "Failed to create anonymous session", sessionResult.cause());
                        return;
                    }

                    JsonObject queryResult = sessionResult.result().getJsonObject(0);
                    if(queryResult == null || queryResult.isEmpty() || queryResult.getString("sessionKey") == null || queryResult.getString("edgeKey") == null) {
                        logger.error(message.correlationID(), message.origin(), 500, "Failed to create anonymous session for an unknown reason");
                        message.fail("Failed to create anonymous session for an unknown reason");
                        return;
                    }

                    message.reply(new JsonObject()
                        .put("userID", "anonymous")
                        .put("userRoles", new JsonArray())
                        .put("sessionTokenHash", sessionTokenHash));
                });
                Map<String, Object> paramMap = new HashMap<>();
                paramMap.put("sessionDocument", database.jsonObjectToParameterMap(sessionDocument));
                paramMap.put("userDocumentID", "users/anonymous");
                database.query(aql_registerAnonymousSession, paramMap, sessionFuture);
            }
            else {
                message.fail(500, "HANDOVER NOT IMPLEMENTED YET");
                return;
            }
        });
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("tokenHashAsID", sessionTokenHashAsID);
        database.query(aql_getUserOfSessionByTokenHash, paramMap, checkTokenFuture);
    }


    @Processor(
            domain = "users",
            version = "1",
            type = "missingAuthMethodConfig",
            description = "Returns the auth method configuration parameters required for processing login requests",
            requires = {
                    @Payload(key = "authMethodIdentifier", type = DataType.JSONObject, description = "Must contain entry 'type' (e.g. 'email') and, if necessary, parameters for identifying the user, e.g. 'emailAddress'")
            },
            provides = {
                    @Payload(key = "authMethodConfig", type = DataType.JSONObject, description = "Auth method parameters"),
            })
    public void processMissingAuthMethodConfig(Message message) {
        JsonObject body = message.getBodyAsJsonObject();

        JsonObject authMethodIdentifier = body.getJsonObject("authMethodIdentifier");
        if(authMethodIdentifier == null || !authMethodIdentifier.containsKey("type")) {
            message.fail(400, "Must provide authMethodIdentifier and include type specification");
            return;
        }

        String authenticationType = authMethodIdentifier.getString("type");
        if("email".equals(authenticationType)) {

            String emailAddress = authMethodIdentifier.getString("emailAddress");
            if(emailAddress == null) {
                message.fail(400, "Must provide 'emailAddress' for identifying the settings for the user");
                return;
            }

            Future<JsonArray> getFuture = Future.future();
            getFuture.setHandler(getResult -> {
                if (getResult.failed()) {
                    logger.error(message.correlationID(), message.origin(), 500, "Failed to get auth method config.", getResult.cause());
                    message.fail("Failed to get auth method config for an unknown reason");
                    return;
                }

                JsonObject result = getResult.result().isEmpty()? null : getResult.result().getJsonObject(0);

                if(result == null || result.isEmpty() || !result.containsKey("hashFactory") || result.getString("hashFactory") == null) {
                    message.fail(404, "No user with this email address as authentication method found");
                    return;
                }
                if(!"verified".equals(result.getString("status")) && config().getJsonObject("userRegistration").getJsonObject("email").getBoolean("isEMailVerificationRequired")) {
                    message.fail(412, "Email address is not verified: " + result.getString("status"));
                    return;
                }

                message.reply(new JsonObject().put("authMethodConfig", result));
            });
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("emailAddress", emailAddress);
            database.query(aql_getAuthMethodConfigByEmail, paramMap, getFuture);
        }
        else {
            logger.warn(message.correlationID(), message.origin(), 501, "Unknown authentication type : " + authenticationType);
            message.fail(501, "AuthType is not known/supported.");
            return;
        }
    }



    @Processor(
            domain = "users",
            version = "1",
            type = "missingUserRegistration",
            description = "Registers a new user in the database and adds corresponding nodes/edges to the database",
            enablingConditionKey = "isUserRegistrationAllowed",
            requires = {
                    @Payload(key = "authMethodConfig", type = DataType.JSONObject, description = "Must contain string 'type' denoting authentication method (currently only 'email' is supported and all required parameters, e.g. password hash, email address, ...")
            },
            optional = {
                    @Payload(key = "userRoles", type = DataType.JSONArray, description = "Roles of the user that is to be registered. Origin must be whitelisted"),
                    @Payload(key = "profileBasic", type = DataType.JSONObject, description = "Key/values to be stored in the user's generic profile, i.e. accessible for all services. E.g. name, gender, ..."),
                    @Payload(key = "profile", type = DataType.JSONObject, description = "Key/values to be stored in the user's service specific profile, i.e. accessible only for the message's origin. Must be structured into subobjects 'private', 'restricted', 'public', 'service'")
            },
            provides = {
                    @Payload(key = "userID", type = DataType.STRING, description = "ID of the user that has been registered"),
                    @Payload(key = "userRoles", type = DataType.STRING, description = "Roles of the user that has been registered")
            })
    public void processMissingUserRegistration(Message message) {
        JsonObject userRegistration = config().getJsonObject("userRegistration");
        if(!userRegistration.getJsonArray("whitelistByServiceOrigin").contains("*")
            && !userRegistration.getJsonArray("whitelistByServiceOrigin").contains(message.origin())) {
            logger.security(message.correlationID(), message.origin(), 403, "Unauthorized attempt to register a user. Check 'userRegistration.whitelistByServiceOrigin' in user service config");
            message.fail(403, "Origin " + message.origin() + " is not allowed to register users." );
            return;
        }

        JsonObject body = message.getBodyAsJsonObject();
        JsonObject authenticationMethod = body.getJsonObject("authMethodConfig");
        if(authenticationMethod == null || !authenticationMethod.containsKey("type")) {
            message.fail(400, "Must provide authMethodConfig");
            return;
        }
        JsonArray userRoles = body.containsKey("userRoles")? body.getJsonArray("userRoles"): new JsonArray();
        if(!userRoles.isEmpty()
                && !config().getJsonObject("roleManagement").getJsonArray("whitelistByServiceOrigin").contains("*")
                && !config().getJsonObject("roleManagement").getJsonArray("whitelistByServiceOrigin").contains(message.origin())) {
            logger.security( message.correlationID(), message.origin(), 401, "Unauthorized attempt to register a user with roles. Check 'roleManagement.whitelistByServiceOrigin' in user service config");
            message.fail(401, "Origin " + message.origin() + " is not allowed to register users with roles." );
            return;
        }

        // prepare general user documents
        JsonObject notification = new JsonObject();
        JsonObject user = body.containsKey("profileBasic")? body.getJsonObject("profileBasic"): new JsonObject();
        JsonObject profile = body.containsKey("profile")? body.getJsonObject("profile"): new JsonObject()
                .put("private", new JsonObject())
                .put("restricted", new JsonObject()
                .put("public", new JsonObject())
                .put("service", new JsonObject()));

        if(!profile.isEmpty()
            && !profile.containsKey("private")
            && !profile.containsKey("restricted")
            && !profile.containsKey("public")
            && !profile.containsKey("service")) {
            message.fail(400, "Provided profile is not structured into private/restricted/public/service!" );
            return;
        }
        String userID = UUID.randomUUID().toString();
        user.put("userID", userID)
            .put("_key", userID);

        String authenticationType = authenticationMethod.getString("type");

        if("email".equals(authenticationType)) {
            String emailAddress = authenticationMethod.getString("emailAddress");
            String passwordHash = authenticationMethod.getString("passwordHash");
            String hashFactory = authenticationMethod.getString("hashFactory");
            String hashSalt = authenticationMethod.getString("hashSalt");
            Integer hashIterations = authenticationMethod.getInteger("hashIterations");
            Integer hashLength = authenticationMethod.getInteger("hashLength");

            if(emailAddress == null || emailAddress.isEmpty() || !emailPattern.matcher(emailAddress).matches()) {
                message.fail(400, "authenticationMethod must contain entry 'emailAddress' with the user's email address and confirm to the configured valid mail pattern. Provided mail is empty or invalid: " + emailAddress);
                return;
            }
            if(passwordHash == null || passwordHash.isEmpty()) {
                message.fail(400, "authenticationMethod must contain entry 'passwordHash' with the user's password hash");
                return;
            }
            if(hashFactory == null || hashFactory.isEmpty()) {
                message.fail(400, "authenticationMethod must contain entry 'hashFactory' with the used password key factory's name");
                return;
            }
            if(hashSalt == null || hashSalt.isEmpty()) {
                message.fail(400, "authenticationMethod must contain entry 'hashSalt' with the used password salt");
                return;
            }
            if(hashSalt == null || hashSalt.isEmpty()) {
                message.fail(400, "authenticationMethod must contain entry 'hashSalt' with the used password salt");
                return;
            }
            if(hashIterations == null || hashIterations < config().getJsonObject("hashPolicy").getInteger("minimumIterations")) {
                message.fail(400, "authenticationMethod must contain entry 'hashIterations' defining at least " + config().getJsonObject("hashPolicy").getInteger("minimumIterations")  + " iterations. More is better");
                return;
            }
            if(hashLength == null || hashLength < config().getJsonObject("hashPolicy").getInteger("minimumLength")) {
                message.fail(400, "authenticationMethod must contain entry 'hashLength' defining a length of at least " + config().getJsonObject("hashPolicy").getInteger("minimumLength")  + ". More is better");
                return;
            }

            // extend the documents according to available data
            notification.put("emailAddress", emailAddress);
            authenticationMethod.put("status", "unverified");

            // check if user's email address is already known
            Future<JsonArray> checkFuture = Future.future();
            checkFuture.setHandler(checkResult -> {
                if(checkResult.failed()) {
                    logger.error(message.correlationID(), message.origin(), 500, "Failed to register user. Checking for its mail failed.", checkResult.cause());
                    message.fail("Failed to register user for an unknown reason");
                    return;
                }

                JsonObject checkMailResult = checkResult.result().getJsonObject(0);
                if(checkMailResult != null) {
                    if (checkMailResult.containsKey("byNotifications") && !checkMailResult.getJsonArray("byNotifications").isEmpty()) {
                        logger.warn(message.correlationID(), emailAddress, 409, "Rejected user registration attempt as email is already used by another user for notifications");
                        message.reply(409, "Email address already in use for notifications by another user account");
                        return;
                    }
                    if (checkMailResult.containsKey("byEmailAuths") && !checkMailResult.getJsonArray("byEmailAuths").isEmpty()) {
                        logger.warn(message.correlationID(), emailAddress, 409, "Rejected user registration attempt as email is already used by another user for authentication");
                        message.reply(409, "Email address already in use for authentication by another user account");
                        return;
                    }
                }

                // register user
                Future<JsonArray> registerFuture = Future.future();
                registerFuture.setHandler(registerResult -> {
                    if(registerResult.failed()) {
                        String errorMessage = registerResult.cause().getMessage();
                        if(errorMessage != null && errorMessage.contains("409") && errorMessage.contains("1210")) {
                            message.fail(409,"userID already exists");
                        }
                        else {
                            logger.error(message.correlationID(), message.origin(), 500, "Failed to register user", registerResult.cause());
                            message.fail("Failed to register user for an unknown reason");
                        }
                        return;
                    }

                    JsonObject result = registerResult.result().getJsonObject(0);

                    // check if all documents had been created
                    if(!result.getJsonObject("user").containsKey("id")
                            || !result.getJsonObject("notification").containsKey("id")
                            || !result.getJsonObject("profile").containsKey("id")
                            || !result.getJsonObject("auth").containsKey("id")
                            || !result.getJsonObject("authEdge").containsKey("id")
                            || !result.getJsonObject("profileEdge").containsKey("id")
                            || !result.getJsonObject("notificationEdge").containsKey("id")
                            ) {
                        logger.error(message.correlationID(), message.origin(), 500, "Failed to register new user with email " + emailAddress + ". Not all required db elements were created. Please investigate: " + result.encodePrettily());
                        message.fail(500, "Registration incomplete due to an unknown internal error!");
                        return;
                    }
                    // check that all roles have been linked
                    if(!userRoles.isEmpty() && result.getJsonArray("roles").size() != userRoles.size()) {
                        logger.error(message.correlationID(), message.origin(), 500, "Failed to register new user with email " + emailAddress + ". Not all required roles were linked. Please investigate: " + result.encodePrettily());
                        message.fail(500, "Registration incomplete due to an unknown internal error!");
                        return;
                    }

                    JsonObject replyMessage = new JsonObject()
                            .put("userID", userID)
                            .put("userRoles", userRoles);

                    if(userRegistration.getJsonObject("email").getBoolean("isEMailVerificationRequired")) {
                        // TODO: send mail for verification and store key in DB
                        logger.fatal(message.correlationID(), "Mail verification not impl yet");
                        replyMessage.put("status", "deactivated")
                            .put("message", "Missing email verification");
                    }
                    else {
                        replyMessage.put("status", "active");
                    }

                    message.reply(200, replyMessage);
                });
                Map<String, Object> paramMap = new HashMap<>();
                paramMap.put("userDocument", database.jsonObjectToParameterMap(user));
                paramMap.put("notificationDocument", database.jsonObjectToParameterMap(notification));
                paramMap.put("profileDocument", database.jsonObjectToParameterMap(profile));
                paramMap.put("authenticationDocument", database.jsonObjectToParameterMap(authenticationMethod));
                paramMap.put("authType", "email");
                paramMap.put("origin", message.origin());
                paramMap.put("notificationType", "email");

                if(userRoles.isEmpty()) {
                    database.query(aql_registerUserByEmail, paramMap, registerFuture);
                }
                else if(config().getJsonObject("roleManagement").getBoolean("isOnTheFlyRoleCreationAllowed")) {
                    paramMap.put("roleNames", database.jsonArrayToParameterArray(userRoles));
                    database.query(aql_registerUserByEmailWithRolesOTF, paramMap, registerFuture);
                }
                else {
                    message.fail("Role management is not implemented yet");
                }
            });
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("emailAddress", emailAddress);
            database.query(aql_checkIfEmailAddressIsUsed, paramMap, checkFuture);
        }
        else {
            logger.warn(message.correlationID(), message.origin(), 501, "Unknown authentication type : " + authenticationType);
            message.fail(501, "AuthType is not known/supported.");
            return;
        }
    }

    @Override
    public void prepare(Future<Object> prepareFuture) {
        emailPattern = Pattern.compile("^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$", Pattern.CASE_INSENSITIVE);

        prepareFuture.complete();
    }

    @Override
    public void startConsuming() {

    }

    @Override
    public void shutdown(Future<Object> shutdownFuture) {
        shutdownFuture.complete();
    }
}
